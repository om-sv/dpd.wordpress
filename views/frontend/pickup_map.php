<!DOCTYPE>
<html>
<head>
    <meta charset="UTF-8">
    <title>DPD pickup map widget</title>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
    <link rel="stylesheet" href="<?php echo DPD_PLUGIN_URI; ?>assets/js/dpd.widgets.map/src/css/style.css?v1">
    <link rel="stylesheet" href="<?php echo DPD_PLUGIN_URI; ?>assets/js/dpd-pickpoint-widget/css/iframe.css">
    <script src="<?php echo DPD_PLUGIN_URI; ?>assets/js/dpd.widgets.map/src/js/jquery.min.js"></script>
    <script src="<?php echo DPD_PLUGIN_URI; ?>assets/js/dpd.widgets.map/src/js/jquery.dpd.map.js?v2"></script>
    <script>
        $(function() {
            'use strict';

            setTimeout(function(){
                $('#dpd-map').dpdMap(
                    {
                        placemarkIcon : '<?php echo DPD_PLUGIN_URI; ?>assets/js/dpd.widgets.map/assets/img/pickup_locationmarker.png',
                        placemarkIconActive : '<?php echo DPD_PLUGIN_URI; ?>assets/js/dpd.widgets.map/assets/img/pickup_locationmarker_highlighted.png',
                    }, 
                    <?php echo json_encode([
                        'tariffs'   => $tariffs,
                        'terminals' => $terminals
                    ]) ?>)
                .on('dpd.map.terminal.select', function(e, terminal, widget) {
                    parent.postMessage({type:'select', data: terminal}, '*');
                })

                $('#cancel').click(function(){
                    parent.postMessage({type:'close'}, '*');
                });

                // hack
                setTimeout(function() {
                    $('#dpd-map').data('dpd.map').setCenter();
                }, 1000);
                
            }, 500);
        })
    </script>
</head>
<body style="margin:0">
    <div id="dpd-map"></div>
    <a href="#" id="cancel">&#10005;</a>
</body>
</html>