<table class="form-table dpd">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_recipient_fio">
                <?php echo __('Contact person', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="order[recipient][fio]"
                    value="<?php 
                        echo $recipient['fio'];
                    ?>"
                    id="dpd_recipient_fio" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_recipient_name">
                <?php echo __('Contact name', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="order[recipient][name]"
                    value="<?php 
                        echo $recipient['name'];
                    ?>"
                    id="dpd_recipient_name" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_recipient_phone">
                <?php echo __('Phone', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="order[recipient][phone]"
                    value="<?php 
                        echo $recipient['phone'];
                    ?>"
                    id="dpd_recipient_phone" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_recipient_email">
                <?php echo __('Email', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="email"
                    name="order[recipient][email]"
                    value="<?php 
                        echo $recipient['email'];
                    ?>"
                    id="dpd_recipient_email" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_recipient_need_pass">
                <?php echo __('Require Pass', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_recipient_need_pass">
                <input class="" type="checkbox" name="order[recipient][need_pass]"
                    id="dpd_recipient_need_pass" value="1" 
                    <?php 
                        echo $recipient['need_pass'] ?
                            'checked="checked"' : ''; 
                    ?>
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
            </fieldset>
        </td>
    </tr> 
</table>
<h3><?php echo __('Location', 'dpd'); ?></h3>
<table class="form-table dpd">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_recipient_city">
                <?php echo __('City', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <?php
                    echo $recipient['location'];
                ?>
            </fieldset>
        </td>
    </tr>
</table>
<div class="tab-wrapper order">
    <div class="dpd-tab-content-2" id="dpd_door" 
        <?php echo $deliveryVariant == 'ДТ' || $deliveryVariant == 'ТТ' ?
            'style="display:none"' : '';
        ?>
    >
        <table class="form-table dpd">
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_recipient_street">
                        <?php echo __('Street', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[recipient][street]"
                            value="<?php 
                                echo $recipient['street'];
                            ?>"
                            id="dpd_recipient_street" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_recipient_streetabbr">
                        <?php echo __('Street abbreviation', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[recipient][streetabbr]"
                            value="<?php 
                                echo $recipient['streetabbr'];
                            ?>"
                            id="dpd_recipient_streetabbr"
                            <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_recipient_house">
                        <?php echo __('Housing', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[recipient][house]"
                            value="<?php 
                                echo $recipient['house'];
                            ?>"
                            id="dpd_recipient_korpus" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_recipient_korpus">
                        <?php echo __('Korpus', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[recipient][korpus]"
                            value="<?php 
                                echo $recipient['korpus'];
                            ?>"
                            id="dpd_recipient_korpus" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_recipient_str">
                        <?php echo __('Structure', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[recipient][str]"
                            value="<?php 
                                echo $recipient['str'];
                            ?>"
                            id="dpd_recipient_str" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_recipient_vlad">
                        <?php echo __('Possession', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[recipient][vlad]"
                            value="<?php 
                                echo $recipient['vlad'];
                            ?>"
                            id="dpd_recipient_vlad" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_recipient_office">
                        <?php echo __('Office', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[recipient][office]"
                            value="<?php 
                                echo $recipient['office'];
                            ?>"
                            id="dpd_recipient_office" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_recipient_flat">
                        <?php echo __('Flat', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[recipient][flat]"
                            value="<?php 
                                echo $recipient['flat'];
                            ?>"
                            id="dpd_recipient_flat" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
        </table>
    </div>
    <div class="dpd-tab-content-2" id="dpd_terminal"
        <?php echo $deliveryVariant == 'ТД' || $deliveryVariant == 'ДД' ?
            'style="display:none"' : '';
        ?>
    >
        <table class="form-table dpd">
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_recipient_terminal_code">
                        <?php echo __('Terminal', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <select name="order[recipient][terminal_code]"
                            id="dpd_recipient_terminal_code" 
                                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                            <?php foreach ($recipientTerminals as $terminal): ?>
                                <option value="<?php echo $terminal['CODE']; ?>" 
                                    <?php if ($recipient['terminal_code'] == $terminal['CODE']): ?> selected="" <?php endif; ?>>
                                    <?php echo $terminal['ADDRESS_SHORT']; ?>
                                </option>
                            <?php endforeach;?>
                        </select>
                        <input type="hidden" name="order[recipient][city_id]"
                            value="<?php echo $recipient['city_id']; ?>">
                    </fieldset>
                </td>
            </tr>
        </table>
    </div>
</div>