<h3><?php echo __('Order content', 'dpd'); ?></h3>

<table class="form-table pay-tab-table">
    <tr>
        <th><?php echo __('Name', 'dpd'); ?></th>
        <th><?php echo __('Quantity', 'dpd'); ?></th>
        <th><?php echo __('Declared cost', 'dpd'); ?></th>
        <th><?php echo __('Npp amount', 'dpd'); ?></th>
        <th><?php echo __('Tax', 'dpd'); ?></th>
    </tr>
    <?php foreach($unitLoads as $key => $ul): ?>
        <tr>
            <td>
                <?php echo $ul['name']; ?>
                <input type="hidden" class="order-pay-tab-input"
                    <?php echo $sended ? 'disabled="disabled"' : ''; ?>
                    name="order[unit_loads][<?php echo $key ?>][name]"
                    value="<?php echo $ul['name'] ?>">
            </td>
            <td>
                <input type="text" class="order-pay-tab-input"
                    <?php echo $sended ? 'disabled="disabled"' : ''; ?>
                    name="order[unit_loads][<?php echo $key ?>][qty]"
                    value="<?php echo $ul['qty'] ?>">
            </td>
            <td><input type="text" class="order-pay-tab-input"
                    <?php echo !$useCargoValue ? 'disabled="disabled"' : ''; ?>
                    <?php echo $sended ? 'disabled="disabled"' : ''; ?>
                    name="order[unit_loads][<?php echo $key ?>][declared_value]"
                    value="<?php echo isset($ul['declared_value']) ? $ul['declared_value'] : 0 ?>">
            </td>
            <td><input type="text" class="order-pay-tab-input"
                    <?php echo !$npp ? 'disabled="disabled"' : ''; ?>
                    <?php echo $sended ? 'disabled="disabled"' : ''; ?>
                    name="order[unit_loads][<?php echo $key ?>][npp_amount]"
                    value="<?php echo isset($ul['npp_amount']) ? $ul['npp_amount'] : 0 ?>">
            </td>
            <td>
                <select name="order[unit_loads][<?php echo $key ?>][tax]"
                    <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    <option value="" 
                        <?php echo $ul['tax'] === '' ? 'selected=""' : '' ?>>
                        <?php echo __('Whithout tax', 'dpd'); ?>
                    </option>
                    <option value="0" 
                        <?php echo $ul['tax'] === '0' ? 'selected=""' : '' ?>>
                        <?php echo __('0%', 'dpd'); ?>
                    </option>
                    <option value="10" 
                        <?php echo $ul['tax'] == '10' ? 'selected=""' : '' ?>>
                        <?php echo __('10%', 'dpd'); ?>
                    </option>
                    <option value="20" 
                        <?php echo $ul['tax'] == '20' ? 'selected=""' : '' ?>>
                        <?php echo __('20%', 'dpd'); ?>
                    </option>
                </select>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<h3><?php echo __('Declared value', 'dpd'); ?></h3>
<table class="form-table dpd">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_recipient_fio">
                <?php echo __('Indicate the declared value', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_use_cargo_value">
                <input class="" type="checkbox" name="order[use_cargo_value]"
                    id="dpd_use_cargo_value"
                    <?php 
                        echo $useCargoValue && $cargoRegistered ?
                            'checked="checked"' : ''; 
                    ?>
                <?php echo $sended || !$cargoRegistered ? 'disabled="disabled"' : ''; ?>>
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_cargo_value">
                <?php echo __('Cargo value', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_cargo_value">
                <input class="" type="text" name="order[cargo_value]" readonly=""
                    id="dpd_cargo_value" value="<?php  echo $cargoValue; ?>"
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
            </fieldset>
        </td>
    </tr>
</table>
<h3><?php echo __('C.O.D', 'dpd'); ?></h3>
<table class="form-table dpd">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_npp">
                <?php echo __('Use npp', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_npp">
                <input class="" type="checkbox" name="order[npp]"
                    id="dpd_npp"
                    <?php 
                        echo $npp ?
                            'checked="checked"' : ''; 
                    ?>
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sum_npp">
                <?php echo __('Npp sum', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_sum_npp">
                <input class="" type="text" name="order[sum_npp]" readonly=""
                    id="dpd_sum_npp"  value="<?php echo $sumNpp; ?>"
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
            </fieldset>
        </td>
    </tr>
</table>
