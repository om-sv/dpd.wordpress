<h3><?php echo __('Options', 'dpd'); ?></h3>
<div id="message" class="notice notice-info inline">
    <p><?php echo  __('The cost of the options is not taken into account when calculating the delivery.', 'dpd'); ?></p>
</div>
<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_cargo_registered">
                <?php echo __('Valuable cargo', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_cargo_registered">
                <input class="" type="checkbox" name="order[cargo_registered]"
                    id="dpd_cargo_registered" value="1" 
                    <?php 
                        echo $cargoRegistered ?
                            'checked="checked"' : ''; 
                    ?>
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_dvd">
                <?php echo __('Weekend delivery', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_dvd">
                <input class="" type="checkbox" name="order[dvd]"
                    id="dpd_dvd" value="1" 
                    <?php 
                        echo $dvd ?
                            'checked="checked"' : ''; 
                    ?>
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_trm">
                <?php echo __('Temperature conditions', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_trm">
                <input class="" type="checkbox" name="order[trm]"
                    id="dpd_trm" value="1" 
                    <?php 
                        echo $trm ?
                            'checked="checked"' : ''; 
                    ?>
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_prd">
                <?php echo __('Loading and unloading during delivery', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_prd">
                <input class="" type="checkbox" name="order[prd]"
                    id="dpd_prd" value="1" 
                    <?php 
                        echo $prd ?
                            'checked="checked"' : ''; 
                    ?>
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_vdo">
                <?php echo __('Return documents to the sender', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_vdo">
                <input class="" type="checkbox" name="order[vdo]"
                    id="dpd_vdo" value="1" 
                    <?php 
                        echo $vdo ?
                            'checked="checked"' : ''; 
                    ?>
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_ogd">
                <?php echo __('Waiting on the address', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <select class="dpd-select"
                    type="text" name="order[ogd]"
                    id="dpd_ogd" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    <option value="">
                        <?php echo __('- Not selected -', 'dpd'); ?>
                    </option>
                    <option value="ПРИМ"
                        <?php if ($ogd == 'ПРИМ'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('Fitting', 'dpd'); ?>
                    </option>
                    <option value="ПРОС"
                        <?php if ($ogd == 'ПРОС'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('Simple', 'dpd'); ?>
                    </option>
                    <option value="РАБТ"
                        <?php if ($ogd == 'РАБТ'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('Health check', 'dpd'); ?>
                    </option>
                </select>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
</table>
<h3><?php echo __('Notifications', 'dpd'); ?></h3>
<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_esz">
                <?php echo __('Order Received Email', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_esz">
                    <input type="text" name="order[esz]"
                        id="dpd_esz" value= 
                        "<?php 
                            echo $esz; 
                        ?>" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
            </fieldset>
        </td>
    </tr>
</table>