<table class="form-table dpd">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_fio">
                <?php echo __('Contact person', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="order[sender][fio]"
                    value="<?php 
                        echo $sender['fio'];
                    ?>"
                    id="dpd_sender_fio" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_name">
                <?php echo __('Contact name', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="order[sender][name]"
                    value="<?php 
                        echo $sender['name'];
                    ?>"
                    id="dpd_sender_name" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_phone">
                <?php echo __('Phone', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="order[sender][phone]"
                    value="<?php 
                        echo $sender['phone'];
                    ?>"
                    id="dpd_sender_phone" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_email">
                <?php echo __('Email', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="email"
                    name="order[sender][email]"
                    value="<?php 
                        echo $sender['email'];
                    ?>"
                    id="dpd_sender_email" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_need_pass">
                <?php echo __('Require Pass', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_sender_need_pass">
                <input class="" type="checkbox" name="order[sender][need_pass]"
                    id="dpd_sender_need_pass" value="1" 
                    <?php 
                        echo $sender['need_pass'] ?
                            'checked="checked"' : ''; 
                    ?>
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                </label>
            </fieldset>
        </td>
    </tr> 
</table>
<h3><?php echo __('Location', 'dpd'); ?></h3>
<table class="form-table dpd">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_city">
                <?php echo __('City', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <?php echo get_option('dpd_sender_city'); ?>
            </fieldset>
        </td>
    </tr>
</table>
<div class="tab-wrapper order">
    <div class="dpd-tab-content-2" id="dpd_door" 
        <?php echo $deliveryVariant == 'ТД' || $deliveryVariant == 'ТТ' ?
            'style="display:none"' : '';
        ?>
    >
        <table class="form-table dpd">
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_street">
                        <?php echo __('Street', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[sender][street]"
                            value="<?php 
                                echo $sender['street'];
                            ?>"
                            id="dpd_sender_street" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_streetabbr">
                        <?php echo __('Street abbreviation', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[sender][streetabbr]"
                            value="<?php 
                                echo $sender['streetabbr'];
                            ?>"
                            id="dpd_sender_streetabbr" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_house">
                        <?php echo __('Housing', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[sender][house]"
                            value="<?php 
                                echo $sender['house'];
                            ?>"
                            id="dpd_sender_korpus" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_korpus">
                        <?php echo __('Korpus', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[sender][korpus]"
                            value="<?php 
                                echo $sender['korpus'];
                            ?>"
                            id="dpd_sender_korpus" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_str">
                        <?php echo __('Structure', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[sender][str]"
                            value="<?php 
                                echo $sender['str'];
                            ?>"
                            id="dpd_sender_str" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_vlad">
                        <?php echo __('Possession', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[sender][vlad]"
                            value="<?php 
                                echo $sender['vlad'];
                            ?>"
                            id="dpd_sender_vlad" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_office">
                        <?php echo __('Office', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[sender][office]"
                            value="<?php 
                                echo $sender['office'];
                            ?>"
                            id="dpd_sender_office" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_flat">
                        <?php echo __('Flat', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="order[sender][flat]"
                            value="<?php 
                                echo $sender['flat'];
                            ?>"
                            id="dpd_sender_flat" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    </fieldset>
                </td>
            </tr>
        </table>
    </div>
    <div class="dpd-tab-content-2" id="dpd_terminal"
        <?php echo $deliveryVariant == 'ДТ' || $deliveryVariant == 'ДД' ?
            'style="display:none"' : '';
        ?>
    >
        <table class="form-table dpd">
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_terminal_code">
                        <?php echo __('Terminal', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <select name="order[sender][terminal_code]"
                            id="dpd_recipient_terminal_code" 
                                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                            <?php foreach ($senderTerminals as $terminal): ?>
                                <option value="<?php echo $terminal['CODE']; ?>" 
                                    <?php if ($sender['terminal_code'] == $terminal['CODE']): ?> selected="" <?php endif; ?>>
                                    <?php echo $terminal['ADDRESS_SHORT']; ?>
                                </option>
                            <?php endforeach;?>
                        </select>
                        <input type="hidden" name="order[sender][city_id]"
                            value="<?php echo $sender['city_id']; ?>">
                    </fieldset>
                </td>
            </tr>
        </table>
    </div>
</div>