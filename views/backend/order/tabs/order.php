<table class="form-table dpd">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('Status', 'dpd'); ?>
        </th>
        <td class="forminp">
            <span id="dpd_status"><?php echo $status; ?></span>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('Woocommerce ID', 'dpd'); ?>
        </th>
        <td class="forminp">
            <?php echo $order->ID; ?>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('DPD ID', 'dpd'); ?></span>
        </th>
        <td class="forminp">
            <span id="dpd_id"><?php echo $orderNum; ?></span>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('Payment type', 'dpd'); ?>
        </th>
        <td class="forminp">
            <select name="order[payment_type]" <?php echo $sended ? 'disabled="disabled"' : ''; ?> >
                <option 
                    <?php if (!$paymentType): ?>
                        selected=""
                    <?php endif; ?>   
                    value="">
                    <?php echo __('Cashless payments', 'dpd'); ?>
                </option>
                <option
                    <?php if ($paymentType == \Ipol\DPD\Order::PAYMENT_TYPE_OUP): ?>
                        selected=""
                    <?php endif; ?> 
                    value="<?php echo \Ipol\DPD\Order::PAYMENT_TYPE_OUP; ?>">
                    <?php echo __('Payment at the recipient in cash', 'dpd'); ?>
                </option>
                <option
                    <?php if ($paymentType == \Ipol\DPD\Order::PAYMENT_TYPE_OUO): ?>
                        selected=""
                    <?php endif; ?> 
                    value="<?php echo \Ipol\DPD\Order::PAYMENT_TYPE_OUO; ?>">
                    <?php echo __('Payment at the sender in cash', 'dpd'); ?>
                </option>
            </select>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_delivery_type">
                <?php echo __('Delivery type', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <select class="dpd-select"
                type="text" name="order[delivery_type]"
                id="dpd_delivery_type" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                <option
                    <?php if ($deliveryType == 'PCL'): ?>
                        selected=""
                    <?php endif; ?>
                    value="PCL">DPD OPTIMUM</option>
                <option
                    <?php if ($deliveryType == 'CSM'): ?>
                        selected=""
                    <?php endif; ?>
                    value="CSM">DPD Online Express</option>
                <option
                    <?php if ($deliveryType == 'ECN'): ?>
                        selected=""
                    <?php endif; ?>
                    value="ECN">DPD ECONOMY</option>
                <option
                    <?php if ($deliveryType == 'ECU'): ?>
                        selected=""
                    <?php endif; ?>
                    value="ECU">DPD ECONOMY CU</option>
                <option
                    <?php if ($deliveryType == 'NDY'): ?>
                        selected=""
                    <?php endif; ?>
                    value="NDY">DPD EXPRESS</option>
                <option
                    <?php if ($deliveryType == 'MXO'): ?>
                        selected=""
                    <?php endif; ?>
                    value="MXO">DPD Online Max</option>
            </select>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('Delivery variant', 'dpd'); ?>
        </th>
        <td class="forminp">
            <select name="order[delivery_variant]" <?php echo $sended ? 'disabled="disabled"' : ''; ?> >
                <option
                    <?php if ($deliveryVariant == 'ДД'): ?>
                        selected=""
                    <?php endif; ?> 
                    value="ДД">
                    <?php echo __('Door - Door', 'dpd'); ?>
                </option>
                <option
                    <?php if ($deliveryVariant == 'ДТ'): ?>
                        selected=""
                    <?php endif; ?> 
                    value="ДТ">
                    <?php echo __('Door - Terminal', 'dpd'); ?>
                </option>
                <option
                    <?php if ($deliveryVariant == 'ТД'): ?>
                        selected=""
                    <?php endif; ?> 
                    value="ТД">
                    <?php echo __('Terminal - Door', 'dpd'); ?>
                </option>
                <option
                    <?php if ($deliveryVariant == 'ТТ'): ?>
                        selected=""
                    <?php endif; ?> 
                    value="ТТ">
                    <?php echo __('Terminal - Terminal', 'dpd'); ?>
                </option>
            </select>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('Date of shipment to DPD', 'dpd'); ?>
        </th>
        <td class="forminp">
            <input type="text" name="order[pickup_date]" class="datepicker"
                value="<?php echo $pickUpDate; ?>" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('DPD Transit Time Interval', 'dpd'); ?>
        </th>
        <td class="forminp">
            <select name="order[pickup_time_period]" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                <option value="9-18"
                    <?php if ($pickupTimePeriod == '9-18'): ?>
                        selected=""
                    <?php endif; ?>
                >
                    <?php echo __('any time from 09:00 to 18:00', 'dpd'); ?>
                </option>
                <option value="9-13"
                    <?php if ($pickupTimePeriod == '9-13'): ?>
                        selected=""
                    <?php endif; ?>
                >
                    <?php echo __('from 09:00 to 13:00', 'dpd'); ?>
                </option>
                <option value="13-18"
                    <?php if ($pickupTimePeriod == '13-18'): ?>
                        selected=""
                    <?php endif; ?>
                >
                    <?php echo __('13:00 to 18:00', 'dpd'); ?>
                </option>
            </select>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_delivery_time_period">
                <?php echo __('Delivery time interval', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <select class="dpd-select"
                    name="order[delivery_time_period]"
                    id="dpd_delivery_time_period" <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
                    <option value="9-18"
                        <?php if ($deliveryTimePeriod == '9-18'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('any time from 09:00 to 18:00', 'dpd'); ?>
                    </option>
                    <option value="9-14"
                        <?php if ($deliveryTimePeriod == '9-14'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('from 09:00 to 14:00', 'dpd'); ?>
                    </option>
                    <option value="13-18"
                        <?php if ($deliveryTimePeriod == '13-18'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('13:00 to 18:00', 'dpd'); ?>
                    </option>
                    <option value="18-22"
                        <?php if ($deliveryTimePeriod == '18-22'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('18:00 to 22:00 (extra charge)', 'dpd'); ?>
                    </option>
                </select>
            </fieldset>
        </td>
    </tr>
</table>
<h3><?php echo __('Order detail', 'dpd'); ?></h3>
<table class="form-table dpd">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('Shipping weight, kg', 'dpd'); ?>
        </th>
        <td class="forminp">
            <input type="text" name="order[shipping_weight]" value="<?php echo $shippingWeight; ?>"
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('Dimensions', 'dpd'); ?>
        </th>
        <td class="forminp">
            <input type="text" name="order[dimensions_width]" class="dimensions-input"
                value="<?php echo $shippingWidth; ?>" id="dpd_dimensions_width"
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>> x
            <input type="text" name="order[dimensions_height]" class="dimensions-input" 
                value="<?php echo $shippingHeight; ?>" id="dpd_dimensions_height"
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>> x
            <input type="text" name="order[dimensions_length]" class="dimensions-input"
                value="<?php echo $shippingLength; ?>" id="dpd_dimensions_length"
                <?php echo $sended ? 'disabled="disabled"' : ''; ?>>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('Volume, m<sup>3</sup>', 'dpd'); ?>
        </th>
        <td class="forminp">
            <input type="text" name="order[cargo_volume]" value="<?php echo $cargoVolume; ?>"
                id="dpd_cargo_volume" readonly="" <?php echo $sended ? 'disabled="disabled"' : ''; ?>> 
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('Number of cargo spaces (parcels)', 'dpd'); ?>
        </th>
        <td class="forminp">
            <input type="text" name="order[cargo_num_pack]" value="<?php echo $cargoNumPack; ?>"
            <?php echo $sended ? 'disabled="disabled"' : ''; ?> >
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <?php echo __('Content Submission', 'dpd'); ?>
        </th>
        <td class="forminp">
            <input type="text" name="order[content_submission]" 
                value="<?php echo $contentSubmission; ?>"
                <?php echo $sended ? 'disabled="disabled"' : ''; ?> >
        </td>
    </tr>
</table>
<script>
    jQuery('.datepicker').datepicker({
        minDate: new Date()
    });
    // jQuery('#dpd_dimensions_width,'+
    //     '#dpd_dimensions_height, #dpd_dimensions_length').keyup(function() {
    //     jQuery('#dpd_cargo_volume').val((
    //             (parseFloat(jQuery('#dpd_dimensions_width').val()))
    //             * (parseFloat(jQuery('#dpd_dimensions_height').val()))
    //             * (parseFloat(jQuery('#dpd_dimensions_length').val()))
    //             / 1000000).toFixed(6));
    // });
</script>