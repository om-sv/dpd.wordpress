<?php
    $subTabActive = isset($_GET['dpd-subtab']) ? $_GET['dpd-subtab'] : 'RU';
    $tabs = [
        'RU' => __('Russia', 'dpd'),
        'KZ' => __('Kazakhstan', 'dpd'),
        'BY' => __('Belarus', 'dpd')
    ];
    $dpdAccountDefaultCountry = get_option('dpd_account_default_country') ?
        get_option('dpd_account_default_country') : 'RU';
?>
<div id="message" class="notice notice-info inline">
    <p><?php echo  __('You can get the integration key by following the link <a href="http://www.dpd.ru/ols/order/personal/integrationkey.do2" target="_blank">http://www.dpd.ru/ols/order/personal/integrationkey.do2</a> in your MyDPD account or by contacting the support service by email itcustomers@dpd.ru', 'dpd'); ?></p>
</div>
<nav class="nav-tab-wrapper woo-nav-tab-wrapper" data-tabs-content-level="2">
    <?php foreach($tabs as $id => $tabname): ?>
        <a href="#" class="nav-tab dpd-tab 
            <?php echo $id == $subTabActive ? 'nav-tab-active' : ''; ?>"
            data-tab-content-id="dpd_<?php echo $id; ?>">
            <?php echo $tabname; ?>
        </a>
    <?php endforeach; ?>
</nav>
<div class="tab-wrapper">
    <?php foreach($tabs as $id => $tabname): ?>
        <div class="dpd-tab-content-2" id="dpd_<?php echo $id; ?>"
            <?php 
                echo $id == $subTabActive ? '' : 'style="display:none;"';
            ?>
        >
            <div class="dpd-tab-content-border">
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row" class="titledesc">
                            <label for="dpd_client_number_<?php echo $id; ?>">
                                <?php echo __('Client Number', 'dpd'); ?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <input type="text"
                                    name="dpd[client_number_<?php echo $id; ?>]"
                                    value="<?php 
                                        echo get_option('dpd_client_number_'.$id);
                                    ?>"
                                    id="dpd_client_number_<?php echo $id; ?>">
                            </fieldset>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row" class="titledesc">
                            <label for="dpd_auth_key_<?php echo $id; ?>">
                                <?php echo __('Authorization key', 'dpd'); ?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <input type="text"
                                    name="dpd[auth_key_<?php echo $id; ?>]"
                                    value="<?php 
                                        echo get_option('dpd_auth_key_'.$id);
                                    ?>"
                                    id="dpd_auth_key_<?php echo $id; ?>">
                            </fieldset>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row" class="titledesc">
                            <label for="dpd_currency_<?php echo $id; ?>">
                                <?php echo __('Account currency', 'dpd'); ?>
                            </label>
                        </th>
                        <td class="forminp">
                            <?php
                                $currency = get_option('dpd_currency_'.$id);
                            ?>
                            <fieldset>
                                <select id="dpd_currency_<?php echo $id; ?>"
                                    name="dpd[currency_<?php echo $id; ?>]">
                                    <option value="KZT"
                                        <?php echo $currency == 'KZT' ? 'selected="selected"' : ''; ?>
                                    ><?php echo __('Tenge', 'dpd'); ?></option>
                                    <option value="RUB"
                                        <?php echo $currency == 'RUB' || !$currency  ?
                                            'selected="selected"' : ''; ?>
                                    ><?php echo __('Russian Ruble', 'dpd'); ?></option>
                                    <option value="USD"
                                        <?php echo $currency == 'USD' ? 'selected="selected"' : ''; ?>
                                    ><?php echo __('US Dollar', 'dpd'); ?></option>
                                    <option value="EUR"
                                        <?php echo $currency == 'EUR' ? 'selected="selected"' : ''; ?>
                                    ><?php echo __('Euro', 'dpd'); ?></option>
                                    <option value="UAH"
                                        <?php echo $currency == 'UAH' ? 'selected="selected"' : ''; ?>
                                    ><?php echo __('Hryvnia', 'dpd'); ?></option>
                                    <option value="BYN"
                                        <?php echo $currency == 'BYN' ? 'selected="selected"' : ''; ?>
                                    ><?php echo __('Belarusian ruble', 'dpd'); ?></option>
                                </select>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_account_default_country">
                <?php echo __('Default account', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <select name="dpd[account_default_country]"
                    id="dpd_account_default_country">
                    <option value="RU"
                        <?php 
                            echo $dpdAccountDefaultCountry == 'RU' ?
                                'selected="selected"' : '';
                        ?>
                    >
                        <?php echo __('Russia', 'dpd'); ?>
                    </option>
                    <option value="KZ"
                        <?php 
                            echo $dpdAccountDefaultCountry == 'KZ' ?
                                'selected="selected"' : '';
                        ?>
                    >
                        <?php echo __('Kazakhstan', 'dpd'); ?>
                    </option>
                    <option value="BY"
                        <?php 
                            echo $dpdAccountDefaultCountry == 'BY' ?
                                'selected="selected"' : '';
                        ?>
                    >
                        <?php echo __('Belarus', 'dpd'); ?>
                    </option>
                </select>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_test_mode">
                <?php echo __('Test mode', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_test_mode">
                <input type="hidden" name="dpd[test_mode]" value="0">
                <input class="" type="checkbox" name="dpd[test_mode]"
                    id="dpd_test_mode" value="1" 
                    <?php 
                        echo get_option('dpd_test_mode') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_required_pickpoint_selection">
                <?php echo __('Pick pount selection required', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_required_pickpoint_selection">
                <input class="" type="checkbox" name="dpd[required_pickpoint_selection]"
                    id="dpd_required_pickpoint_selection" value="1" 
                    <?php 
                        echo get_option('dpd_required_pickpoint_selection') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr>
    <!--<tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_not_include_map_apis">
                <?php/* echo __('Do not include the Maps API', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_not_include_map_apis">
                <input class="" type="checkbox" name="dpd[not_include_map_apis]"
                    id="dpd_not_include_map_apis" value="1" 
                    <?php 
                        echo get_option('dpd_not_include_map_apis') ?
                            'checked="checked"' : ''; */
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr>-->
</table>