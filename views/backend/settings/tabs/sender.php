<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_fio">
                <?php echo __('Contact person', 'dpd'); ?>
                <span class="required">*</span>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="dpd[sender_fio]"
                    value="<?php 
                        echo get_option('dpd_sender_fio');
                    ?>"
                    id="dpd_sender_fio">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_name">
                <?php echo __('Contact name', 'dpd'); ?>
                <span class="required">*</span>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="dpd[sender_name]"
                    value="<?php 
                        echo get_option('dpd_sender_name');
                    ?>"
                    id="dpd_sender_name">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_phone">
                <?php echo __('Phone', 'dpd'); ?>
                <span class="required">*</span>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="dpd[sender_phone]"
                    value="<?php 
                        echo get_option('dpd_sender_phone');
                    ?>"
                    id="dpd_sender_phone">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_email">
                <?php echo __('Email', 'dpd'); ?>
                <span class="required">*</span>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="email"
                    name="dpd[sender_email]"
                    value="<?php 
                        echo get_option('dpd_sender_email');
                    ?>"
                    id="dpd_sender_email">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_regular_num">
                <?php echo __('Regular number', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="dpd[sender_regular_num]"
                    value="<?php 
                        echo get_option('dpd_sender_regular_num');
                    ?>"
                    id="dpd_sender_regular_num">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_need_pass">
                <?php echo __('Require Pass', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_sender_need_pass">
                <input class="" type="checkbox" name="dpd[sender_need_pass]"
                    id="dpd_sender_need_pass" value="1" 
                    <?php 
                        echo get_option('dpd_sender_need_pass') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr> 
</table>
<h3><?php echo __('Location', 'dpd'); ?></h3>
<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_sender_city">
                <?php echo __('City', 'dpd'); ?>
                <span class="required">*</span>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text" autocomplete="off"
                    name="dpd[sender_city]"
                    value="<?php 
                        echo get_option('dpd_sender_city');
                    ?>"
                    id="dpd_sender_city">
                <input type="hidden" id="dpd_sender_city_id" name="dpd[sender_city_id]"
                    value="<?php echo get_option('dpd_sender_city_id'); ?>">
            </fieldset>
        </td>
    </tr>
</table>
<nav class="nav-tab-wrapper woo-nav-tab-wrapper" data-tabs-content-level="2">
    <a href="#" class="nav-tab dpd-tab nav-tab-active" data-tab-content-id="dpd_door">
        <?php echo __('To door', 'dpd'); ?>
    </a>
    <a href="#" class="nav-tab dpd-tab" data-tab-content-id="dpd_terminal">
        <?php echo __('Terminal', 'dpd'); ?>
    </a>
</nav>
<div class="tab-wrapper">
    <div class="dpd-tab-content-2" id="dpd_door">
        <table class="form-table">
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_street">
                        <?php echo __('Street', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="dpd[sender_street]"
                            value="<?php 
                                echo get_option('dpd_sender_street');
                            ?>"
                            id="dpd_sender_street">
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_streetabbr">
                        <?php echo __('Street abbreviation', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="dpd[sender_streetabbr]"
                            value="<?php 
                                echo get_option('dpd_sender_streetabbr');
                            ?>"
                            id="dpd_sender_streetabbr">
                    </fieldset>
                </td>
            </tr>
           <tr valign="top">
                 <th scope="row" class="titledesc">
                    <label for="dpd_sender_house">
                        <?php echo __('Housing', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="dpd[sender_house]"
                            value="<?php 
                                echo get_option('dpd_sender_house');
                            ?>"
                            id="dpd_sender_korpus">
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_str">
                        <?php echo __('Korpus', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="dpd[sender_korpus]"
                            value="<?php 
                                echo get_option('dpd_sender_korpus');
                            ?>"
                            id="dpd_sender_str">
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_str">
                        <?php echo __('Structure', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="dpd[sender_str]"
                            value="<?php 
                                echo get_option('dpd_sender_str');
                            ?>"
                            id="dpd_sender_str">
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_vlad">
                        <?php echo __('Possession', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="dpd[sender_vlad]"
                            value="<?php 
                                echo get_option('dpd_sender_vlad');
                            ?>"
                            id="dpd_sender_vlad">
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_office">
                        <?php echo __('Office', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="dpd[sender_office]"
                            value="<?php 
                                echo get_option('dpd_sender_office');
                            ?>"
                            id="dpd_sender_office">
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_flat">
                        <?php echo __('Flat', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <input type="text"
                            name="dpd[sender_flat]"
                            value="<?php 
                                echo get_option('dpd_sender_flat');
                            ?>"
                            id="dpd_sender_flat">
                    </fieldset>
                </td>
            </tr>
        </table>
    </div>
    <div class="dpd-tab-content-2" id="dpd_terminal" style="display: none;">
        <table class="form-table">
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="dpd_sender_terminal_code">
                        <?php echo __('Terminal', 'dpd'); ?>
                    </label>
                </th>
                <td class="forminp">
                    <fieldset>
                        <select name="dpd[sender_terminal_code]"
                            id="dpd_sender_terminal_code" disabled="">
                        </select>
                    </fieldset>
                </td>
            </tr>
        </table>
    </div>
</div>
<script>
    var senderTerminalCode = '<?php echo get_option('dpd_sender_terminal_code'); ?>';
</script>