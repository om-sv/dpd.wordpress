<div id="message" class="notice notice-info inline">
    <p><?php echo  __('This group of settings is needed in order to quickly track the status of orders. Once every 10 minutes, information is requested on the status of sent applications. When a response is received, orders will be placed in the specified statuses if they are accepted, or for some reason rejected. It also tracks the status of delivery orders. It is recommended to create two new ones.
order status, to make it easier to track the status of applications.', 'dpd'); ?>
    </p>
    <p>
        <?php echo  __('<strong>Attention!</strong> To be able to track the statuses of the DPD packages, you need to make a request to the DPD employee on the dpd.ru website. To do this, write to him: "To work with the getStatesByClient method, connect us with the package tracking service with confirmation"', 'dpd'); ?>
    </p>
</div>
<?php
    $dpdOrderStatuses = \Ipol\DPD\DB\Order\Model::StatusList();
    $wcOrderStatuses = wc_get_order_statuses();
?>
<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_set_tracking_number">
                <?php echo __('Place orders for a shipment ID', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_set_tracking_number">
                <input class="" type="checkbox" name="dpd[set_tracking_number]"
                    id="dpd_cargo_registered" value="1" 
                    <?php 
                        echo get_option('dpd_set_tracking_number') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_mark_payed">
                <?php echo __('Mark delivered order paid', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_mark_payed">
                <input class="" type="checkbox" name="dpd[mark_payed]"
                    id="dpd_dvd" value="1" 
                    <?php 
                        echo get_option('dpd_mark_payed') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_status_order_check">
                <?php echo __('Track order statuses in DPD', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_status_order_check">
                <input class="" type="checkbox" name="dpd[status_order_check]"
                    id="dpd_status_order_check" value="1" 
                    <?php 
                        echo get_option('dpd_status_order_check') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr>
    <?php foreach ($dpdOrderStatuses as $id => $status): ?>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="dpd_<?php echo $id; ?>">
                    <?php echo $status; ?>
                </label>
            </th>
            <td class="forminp">
                <fieldset>
                    <select class="dpd-select status" 
                    <?php echo get_option('dpd_status_order_check') ?
                        '' : 'disabled';
                    ?>
                        type="text" name="dpd[sync_order_status_<?php echo $id; ?>]"
                        id="dpd_<?php echo $id; ?>">
                        <option value=""><?php echo __('- No binding -', 'dpd'); ?></option>

                        <?php foreach ($wcOrderStatuses as $key => $wcstatus): ?>
                            <option value="<?php echo $key; ?>"
                                <?php echo get_option('dpd_sync_order_status_'.$id) == $key ? 'selected=""' : ''; ?>>
                                <?php echo $wcstatus; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </fieldset>
            </td>
        </tr>
    <?php endforeach; ?>
</table>