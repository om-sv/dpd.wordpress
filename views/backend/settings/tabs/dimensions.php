<!--<div id="message" class="notice notice-info inline">
    <p><?php /*echo  __('This group of settings is intended to determine the dimensions of those orders where there are goods without filled dimensions and / or weight. Here you can specify the values that will be taken as default for an order or product.', 'dpd'); ?></p>
    <p>
        <strong><?php echo  __('Apply to the entire order', 'dpd'); ?></strong> - 
        <?php echo  __('A cumulative calculation of the dimensions of the entire order will be made, the total size and weight of the order will be checked and that value will be taken - calculated or set by default - which will be larger.', 'dpd'); ?>
        
    </p>
    <p>
        <strong><?php echo  __('Apply for goods in order', 'dpd'); ?></strong> - 
        <?php echo  __('In this case, when calculating dimensions, if the goods do not specify a value of one or several dimensions, it will be taken from the default values.', 'dpd'); */?>
        
    </p>
</div>-->
<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_use_mode">
                <?php echo __('Apply', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <select type="text" name="dpd[use_mode]" id="dpd_use_mode">
                    <option value="ORDER"
                        <?php echo 
                            get_option('dpd_use_mode') == 'ORDER' ? 
                                'selected="selected"' : '';
                        ?>
                    >
                        <?php echo __('to the entire order', 'dpd'); ?>
                    </option>
                    <option value="ITEM"
                        <?php echo 
                            get_option('dpd_use_mode') == 'ITEM' ? 
                                'selected="selected"' : '';
                        ?>
                    >
                        <?php echo __('for goods in order', 'dpd'); ?>
                    </option>
                </select>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_weight_default">
                <?php echo __('Weight default, g', 'dpd'); ?>
                <span class="required">*</span>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="dpd[weight_default]"
                    value="<?php 
                        echo get_option('dpd_weight_default');
                    ?>"
                    id="dpd_weight_default">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_length_default">
                <?php echo __('Length by default, mm', 'dpd'); ?>
                <span class="required">*</span>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="dpd[length_default]"
                    value="<?php 
                        echo get_option('dpd_length_default');
                    ?>"
                    id="dpd_length_default">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_width_default">
                <?php echo __('Width by default, mm', 'dpd'); ?>
                <span class="required">*</span>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="dpd[width_default]"
                    value="<?php 
                        echo get_option('dpd_width_default');
                    ?>"
                    id="dpd_width_default">
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_height_default">
                <?php echo __('Height by default, mm', 'dpd'); ?>
                <span class="required">*</span>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <input type="text"
                    name="dpd[height_default]"
                    value="<?php 
                        echo get_option('dpd_height_default');
                    ?>"
                    id="dpd_height_default">
            </fieldset>
        </td>
    </tr>
</table>