<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_ignore_tariff">
                <?php echo __('Specify the rates that will NOT be
used in calculations', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <?php
                    $dpdTarifOff = get_option('dpd_ignore_tariff');
                    if ($dpdTarifOff) {
                        $dpdTarifOff = unserialize($dpdTarifOff);
                    }
                ?>
                <select class="dpd-select"
                    type="text" multiple="" name="dpd[ignore_tariff][]"
                    id="dpd_ignore_tariff">
                    <option></option>
                    <option
                        <?php if ($dpdTarifOff && in_array('PCL', $dpdTarifOff)): ?>
                            selected=""
                        <?php endif; ?>
                        value="PCL">DPD Online Classic</option>
                    <option
                        <?php if ($dpdTarifOff && in_array('CSM', $dpdTarifOff)): ?>
                            selected=""
                        <?php endif; ?>
                        value="CSM">DPD Online Express</option>
                    <option
                        <?php if ($dpdTarifOff && in_array('ECN', $dpdTarifOff)): ?>
                            selected=""
                        <?php endif; ?>
                        value="ECN">DPD ECONOMY</option>
                    <option
                        <?php if ($dpdTarifOff && in_array('ECU', $dpdTarifOff)): ?>
                            selected=""
                        <?php endif; ?>
                        value="ECU">DPD ECONOMY CU</option>
                    <option
                        <?php if ($dpdTarifOff && in_array('NDY', $dpdTarifOff)): ?>
                            selected=""
                        <?php endif; ?>
                        value="NDY">DPD EXPRESS</option>
                    <option
                        <?php if ($dpdTarifOff && in_array('MXO', $dpdTarifOff)): ?>
                            selected=""
                        <?php endif; ?>
                        value="MXO">DPD Online Max</option>
                </select>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_tariff_default">
                <?php echo __('Default rate', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <select class="dpd-select"
                    type="text" name="dpd[tariff_default]"
                    id="dpd_tariff_default">
                    <option value="PCL">DPD Online Classic</option>
                    <option value="CSM">DPD Online Express</option>
                    <option value="ECN">DPD ECONOMY</option>
                    <option value="ECU">DPD ECONOMY CU</option>
                    <option value="NDY">DPD EXPRESS</option>
                    <option value="MXO">DPD Online Max</option>
                </select>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_default_tariff_treshold">
                <?php echo __('Maximum shipping cost
at which the default rate will be applied', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_default_tariff_treshold">
                <input type="text" name="dpd[default_tariff_treshold]"
                    id="dpd_default_tariff_treshold" value= 
                    "<?php 
                        echo get_option('dpd_default_tariff_treshold'); 
                    ?>">
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_declared_value">
                <?php echo __('Include insurance in the cost of delivery', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_declared_value">
                <input class="" type="checkbox" name="dpd[declared_value]"
                    id="dpd_declared_value" value="1" 
                    <?php 
                        echo get_option('dpd_declared_value') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_calculate_by_parcel">
                <?php echo __('Calculate shipping costs by item', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_calculate_by_parcel">
                <input class="" type="checkbox" name="dpd[calculate_by_parcel]"
                    id="dpd_calculate_by_parcel" value="1" 
                    <?php 
                        echo get_option('dpd_calculate_by_parcel') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_round_to">
                <?php echo __('Round to units', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_round_to">
                <input type="text" name="dpd[round_to]"
                    id="dpd_round_to" value= 
                    "<?php 
                        echo get_option('dpd_round_to'); 
                    ?>">
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_add_delivery_day">
                <?php echo __('Extend delivery time (days)', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_add_delivery_day">
                <input type="text" name="dpd[add_delivery_day]"
                    id="dpd_add_delivery_day" value= 
                    "<?php 
                        echo get_option('dpd_add_delivery_day');
                    ?>">
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_commission_npp_check">
                <?php 
                    echo __('Include cash on delivery fee in the cost of delivery', 'dpd'); 
                ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_commission_npp_check">
                <input class="" type="checkbox" name="dpd[commission_npp_check]"
                    id="dpd_commission_npp_check" value="1" 
                    <?php 
                        echo get_option('dpd_commission_npp_check') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_commission_npp_percent">
                <?php echo __('Commission on the value of goods in the order, %', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_commission_npp_percent">
                <input type="text" name="dpd[commission_npp_percent]"
                    id="dpd_commission_npp_percent" value= 
                    "<?php 
                        echo get_option('dpd_commission_npp_percent'); 
                    ?>">
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_commission_npp_minsum">
                <?php echo __('The minimum amount of commission in the currency', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_commission_npp_percent">
                <input type="text" name="dpd[commission_npp_minsum]"
                    id="dpd_commission_npp_percent" value= 
                    "<?php 
                        echo get_option('dpd_commission_npp_minsum'); 
                    ?>">
                </label>
            </fieldset>
        </td>
    </tr>
    <?php
        $gateways = WC()->payment_gateways->get_available_payment_gateways();
        $enabledGateways = [];
        if($gateways) {
            foreach($gateways as $gateway) {
                if($gateway->enabled == 'yes') {
                    $enabledGateways[] = $gateway;
                }
            }
        }
        $сommissionNppPayment = get_option('dpd_commission_npp_payment');
        if ($сommissionNppPayment) {
            $сommissionNppPayment = unserialize($сommissionNppPayment);
        }
    ?>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_commission_npp_payment">
                <?php echo __('Tie payment systems, which means that the payment will be cash on delivery', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <select class="dpd-select"
                    type="text" multiple="" name="dpd[commission_npp_payment][]"
                    id="dpd_commission_npp_payment">
                    <?php foreach($enabledGateways as $gateway): ?>
                        <option value="<?php echo $gateway->id; ?>"
                            <?php if (is_array($сommissionNppPayment) && in_array($gateway->id, $сommissionNppPayment)): ?>
                                selected="selected"
                            <?php endif; ?>
                            >
                            <?php echo $gateway->title; ?>   
                        </option>
                    <?php endforeach; ?>
                </select>
            </fieldset>
        </td>
    </tr>
</table>