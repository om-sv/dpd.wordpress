<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_self_pickup">
                <?php echo __('Sending method', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <?php
                    $dpdSelfPickup = 
                        get_option('dpd_self_pickup');
                ?>
                <select class="dpd-select"
                    type="text" name="dpd[self_pickup]"
                    id="dpd_self_pickup">
                    <option value="1"
                        <?php if ($dpdSelfPickup == '1'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('We will carry orders ourselves', 'dpd'); ?>
                    </option>
                    <option value="0"
                        <?php if ($dpdSelfPickup == '0'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('We want to call the fence automatically', 'dpd'); ?>
                    </option>
                </select>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_payment_type">
                <?php echo __('Payment method delivery', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <?php
                    $dpdPaymentType = 
                        get_option('dpd_payment_type');
                ?>
                <select class="dpd-select"
                    type="text" name="dpd[payment_type]"
                    id="dpd_payment_type">
                    <option value=""
                        <?php if ($dpdPaymentType == ''): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('By sender by wire transfer', 'dpd'); ?>
                    </option>
                    <option value="<?php echo \Ipol\DPD\Order::PAYMENT_TYPE_OUP; ?>"
                        <?php if ($dpdPaymentType == \Ipol\DPD\Order::PAYMENT_TYPE_OUP): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('Payment at the recipient in cash', 'dpd'); ?>
                    </option>
                    <option value="<?php echo \Ipol\DPD\Order::PAYMENT_TYPE_OUO; ?>"
                        <?php if ($dpdPaymentType == \Ipol\DPD\Order::PAYMENT_TYPE_OUO): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('Payment at the sender in cash', 'dpd'); ?>
                    </option>
                </select>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_pickup_time_period">
                <?php echo __('DPD Transit Time Interval', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <?php
                    $dpdPickupTimePeriod = 
                        get_option('dpd_pickup_time_period');
                ?>
                <select class="dpd-select"
                    type="text" name="dpd[pickup_time_period]"
                    id="dpd_pickup_time_period">
                    <option value="9-18"
                        <?php if ($dpdPickupTimePeriod == '9-18'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('any time from 09:00 to 18:00', 'dpd'); ?>
                    </option>
                    <option value="9-13"
                        <?php if ($dpdPickupTimePeriod == '9-13'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('from 09:00 to 13:00', 'dpd'); ?>
                    </option>
                    <option value="13-18"
                        <?php if ($dpdPickupTimePeriod == '13-18'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('13:00 to 18:00', 'dpd'); ?>
                    </option>
                </select>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_delivery_time_period">
                <?php echo __('Delivery time interval', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <?php
                    $dpdDeliveryTimePeriod = 
                        get_option('dpd_delivery_time_period');
                ?>
                <select class="dpd-select"
                    type="text" name="dpd[delivery_time_period]"
                    id="dpd_delivery_time_period">
                    <option value="9-18"
                        <?php if ($dpdDeliveryTimePeriod == '9-18'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('any time from 09:00 to 18:00', 'dpd'); ?>
                    </option>
                    <option value="9-14"
                        <?php if ($dpdDeliveryTimePeriod == '9-14'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('from 09:00 to 14:00', 'dpd'); ?>
                    </option>
                    <option value="13-18"
                        <?php if ($dpdDeliveryTimePeriod == '13-18'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('13:00 to 18:00', 'dpd'); ?>
                    </option>
                    <option value="18-22"
                        <?php if ($dpdDeliveryTimePeriod == '18-22'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('18:00 to 22:00 (extra charge)', 'dpd'); ?>
                    </option>
                </select>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_cargo_num_pack">
                <?php echo __('Number of cargo spaces (parcels)', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_cargo_num_pack">
                <input type="text" name="dpd[cargo_num_pack]"
                    id="dpd_cargo_num_pack" value= 
                    "<?php 
                        echo get_option('dpd_cargo_num_pack'); 
                    ?>">
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_cargo_category">
                <?php echo __('Sending content', 'dpd'); ?>
                <span class="required">*</span>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_cargo_category">
                <input type="text" name="dpd[cargo_category]"
                    id="dpd_cargo_category" value= 
                    "<?php 
                        echo get_option('dpd_cargo_category'); 
                    ?>">
                </label>
            </fieldset>
        </td>
    </tr>
</table>
<h3><?php echo __('Options', 'dpd'); ?></h3>
<div id="message" class="notice notice-info inline">
    <p><?php echo  __('The cost of the options is not taken into account when calculating the delivery.', 'dpd'); ?></p>
</div>
<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_cargo_registered">
                <?php echo __('Valuable cargo', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_cargo_registered">
                <input class="" type="checkbox" name="dpd[cargo_registered]"
                    id="dpd_cargo_registered" value="1" 
                    <?php 
                        echo get_option('dpd_cargo_registered') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_dvd">
                <?php echo __('Weekend delivery', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_dvd">
                <input class="" type="checkbox" name="dpd[dvd]"
                    id="dpd_dvd" value="1" 
                    <?php 
                        echo get_option('dpd_dvd') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_trm">
                <?php echo __('Temperature conditions', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_trm">
                <input class="" type="checkbox" name="dpd[trm]"
                    id="dpd_trm" value="1" 
                    <?php 
                        echo get_option('dpd_trm') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_prd">
                <?php echo __('Loading and unloading during delivery', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_prd">
                <input class="" type="checkbox" name="dpd[prd]"
                    id="dpd_prd" value="1" 
                    <?php 
                        echo get_option('dpd_prd') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_vdo">
                <?php echo __('Return documents to the sender', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_vdo">
                <input class="" type="checkbox" name="dpd[vdo]"
                    id="dpd_vdo" value="1" 
                    <?php 
                        echo get_option('dpd_vdo') ?
                            'checked="checked"' : ''; 
                    ?>
                >
                </label>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_ogd">
                <?php echo __('Waiting on the address', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <?php
                    $dpdOGD = 
                        get_option('dpd_ogd');
                ?>
                <select class="dpd-select"
                    type="text" name="dpd[ogd]"
                    id="dpd_ogd">
                    <option value="0">
                        <?php echo __('- Not selected -', 'dpd'); ?>
                    </option>
                    <option value="ПРИМ"
                        <?php if ($dpdOGD == 'ПРИМ'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('Fitting', 'dpd'); ?>
                    </option>
                    <option value="ПРОС"
                        <?php if ($dpdOGD == 'ПРОС'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('Simple', 'dpd'); ?>
                    </option>
                    <option value="РАБТ"
                        <?php if ($dpdOGD == 'РАБТ'): ?>
                            selected=""
                        <?php endif; ?>
                    >
                        <?php echo __('Health check', 'dpd'); ?>
                    </option>
                </select>
                <small><br><?php echo __('option paid, check with the manager', 'dpd'); ?></small>
            </fieldset>
        </td>
    </tr>
</table>
<h3><?php echo __('Notifications', 'dpd'); ?></h3>
<table class="form-table">
    <tr valign="top">
        <th scope="row" class="titledesc">
            <label for="dpd_esz">
                <?php echo __('Order Received Email', 'dpd'); ?>
            </label>
        </th>
        <td class="forminp">
            <fieldset>
                <label for="dpd_esz">
                    <input type="text" name="dpd[esz]"
                        id="dpd_esz" value= 
                        "<?php 
                            echo get_option('dpd_esz'); 
                        ?>">
                </label>
            </fieldset>
        </td>
    </tr>
</table>