 <?php
    $activeTab = isset($_GET['active_dpd_tab']) ? $_GET['active_dpd_tab'] : 'main';
    
    $tabs = [
        'main' => __('Main settings', 'dpd'),
    ];
    
    if (get_option('dpd_first_data_import_completed')) {
        $tabs['dimensions']           = __('Dimensions', 'dpd');
        $tabs['calculation']          = __('Delivery calculation', 'dpd');
        $tabs['sender']               = __('Sender', 'dpd');
        $tabs['recipient']            = __('Recipient', 'dpd');
        $tabs['shipping_description'] = __('Shipping Description', 'dpd');
        $tabs['status']               = __('Status', 'dpd');
        $tabs['import']               = __('Import data', 'dpd');
    }
?>
    <?php if (!extension_loaded('pdo_mysql')) { ?>
        <div class="error notice is-dismissible">
            <p><?= __('PHP extension pdo_mysql not found', 'dpd') ?></p>
        </div>
    <?php } ?>

    <nav class="nav-tab-wrapper woo-nav-tab-wrapper" data-tabs-content-level="1">
        <?php foreach ($tabs as $id => $tabname): ?>
            <a href="#" class="nav-tab dpd-tab <?php 
                echo $id == $activeTab ? 'nav-tab-active' : ''; ?>"
                data-tab-content-id="<?php echo $id; ?>">
                <?php echo $tabname; ?>     
            </a>
        <?php endforeach; ?>
    </nav>

    <div class="tab-wrapper">
        <?php foreach ($tabs as $id => $tabname): ?>
            <div class="dpd-tab-content-1" id="<?php echo $id; ?>"
                <?php 
                    echo $id == $activeTab ? '' : 'style="display:none;"';
                ?>
            >
            <?php 
                echo \DPD\Helper\View::load('backend/settings/tabs/'.$id); 
            ?>
            </div>
        <?php endforeach; ?>
    </div>