<?php

namespace DPD\Model;

class Model {

    protected $table;
    protected $where;
    protected $order;
    protected $limit;


    public function get()
    {
        $conditions = array_merge($this->where, $this->order ? : [], $this->limit ? : []);
        return $this->table->findModels($conditions);

    }

    public function row()
    {
        $this->limit['limit'] = '0,1';
        $result = $this->get();
        if (isset($result[0])) {
            return $result[0];
        }
        return null;
    }

    public function where($where, $bind)
    {
        $this->where = [
            'where' => $where,
            'bind' => $bind
        ];
    }

    public function order($order)
    {
        $this->order = ['order' => $order];
    }

    public function limit($limit)
    {
        $this->limit = ['limit' => $limit];
    }

}