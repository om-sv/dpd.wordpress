<?php

namespace DPD\Factories;

class Shipment {

    protected $shipment;
    protected $subtotal;
    
    public function __construct(
        \Ipol\DPD\Config\Config $config,
        string $senderCityOption,
        string $recipientCountryCode,
        string $recipientState,
        string $recipientCity,
        string $subtotal
    ) {
        $this->shipment = new \Ipol\DPD\Shipment($config);
        $this->setSender($senderCityOption);
        $this->setReceiver($recipientCountryCode, $recipientState, $recipientCity);
        $this->subtotal = $subtotal;
    }

    protected function setSender($senderCityOption)
    {
        $senderCity = explode(',', $senderCityOption);
        $this->shipment->setSender(
            trim($senderCity[0]),
            trim($senderCity[1]),
            trim($senderCity[2])
        );
    }

    protected function setReceiver($recipientCountryCode, $recipientState, $recipientCity)
    {
        $countryArr = [
            'RU' => 'Россия',
            'BY' => 'Беларусь',
            'KZ' => 'Казахстан'
        ];

        if (in_array($recipientCountryCode, $countryArr)) {
            $recipientCountryCode = array_search($recipientCountryCode, $countryArr);
        }

        $this->shipment->setReceiver(
            isset($countryArr[$recipientCountryCode]) ? $countryArr[$recipientCountryCode] : '',
            $recipientState,
            $recipientCity
        );
    }


    public function setItemsByCart(array $cart)
    {
        $items = [];
        foreach ($cart as $item) {
            $items[] = [
                'NAME' => $item['data']->get_title(),
                'QUANTITY' => $item['quantity'],
                'PRICE' => $item['line_total'] / $item['quantity'],
                'VAT_RATE' => $item['line_tax'],
                'WEIGHT' => (float) $item['data']->get_weight() ? : 0,
                'DIMENSIONS' => [
                    'LENGTH' => (float) $item['data']->get_length() ? : 0,
                    'WIDTH' => (float) $item['data']->get_width() ? : 0,
                    'HEIGHT' => (float) $item['data']->get_height() ? : 0
                ]
            ];
        }
        $this->shipment->setItems($items, $this->subtotal);
    }

    public function setItemsByOrder(array $orderItems)
    {
        $items = [];
        foreach ($orderItems as $item) {
            $product = $item->get_product();
            $items[] = [
                'NAME' => $product->get_title(),
                'QUANTITY' => $item->get_quantity(),
                'PRICE' => $product->get_price(),
                'VAT_RATE' => $item->get_total_tax(),
                'WEIGHT' => (float)$product->get_weight() ? : 0,
                'DIMENSIONS' => [
                    'LENGTH' => (float)$product->get_length() ? : 0,
                    'WIDTH' => (float)$product->get_width() ? : 0,
                    'HEIGHT' => (float)$product->get_height() ? : 0
                ]
            ];
        }
        $this->shipment->setItems($items, $this->subtotal);
    }


    public function getInstance()
    {
        return $this->shipment;
    }
}