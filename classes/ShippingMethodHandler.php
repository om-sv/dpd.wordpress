<?php

namespace DPD;

use WC_Shipping_Method;

use DPD\Model\Terminal as Terminal;
use DPD\Helper\SimpleValidation as SimpleValidation;
use DPD\Helper\PriceRules as PriceRules;
use DPD\Helper\Converter as Converter;
use DPD\Helper\View as View;

use DPD\Factories\Shipment as Shipment;

class ShippingMethodHandler extends WC_Shipping_Method {

    private $DPDconfig;

    public function __construct()
    {
        $this->id = DPD_SHIPPING_METHOD_ID; 
        $this->method_title = __('DPD Shipping', 'dpd');  
        $this->method_description = __('Express delivery of parcels and goods', 'dpd'); 
        $this->availability = 'including';
        $this->countries = [
            'RU',
            'BY',
            'KZ'
        ];

        // $this->supports = array(
        //     'shipping-zones'
        // );

        $this->init();

        $this->enabled = 'yes';
        $this->title = isset($this->settings['title']) ?
            $this->settings['title'] : __('DPD', 'dpd');

        if (isset($_SESSION['success'])) {
            unset($_SESSION['success']);
        }

        if (isset($_SESSION['error'])) {
            unset($_SESSION['error']);
        }

        global $DPDconfig;
        $this->DPDconfig = new \Ipol\DPD\Config\Config($DPDconfig);

    }

    public function init()
    {

        //Загрузка настроек API
        //$this->init_form_fields(); 
        $this->init_settings(); 

        //Инициализация обработчика сохранения настроек
        add_action(
            'woocommerce_update_options_shipping_' . $this->id,
            [$this, 'process_admin_options'] 
        );
    }

    public function admin_options()
    {
        ?>
        <h2><?php echo $this->method_title; ?></h2>
        <p><?php echo $this->method_description; ?></p>
        <?php if (
                    (!get_option('dpd_first_data_import_completed') ||
                        get_option('dpd_run_import_manually')) &&
                        $this->DPDconfig->isActiveAccount()
                ): 
        ?>
            <?php echo View::load('backend/import', [
                'first_run' => true
            ]); ?>
        <?php else: ?>
            <?php $this->generate_settings_html(); ?>
        <?php endif; ?>
        <?php
    }

    /**
     * Генерация страницы настроек
     * @return void
     */
    public function generate_settings_html($form_fields = array(), $echo = true)
    {
        echo View::load('backend/settings/settings');
    }


    /**
     * Расчет доставки
     *
     * @access public
     * @param mixed $package
     * @return void
     */
    public function calculate_shipping($package = array())
    {
        if (!$this->DPDconfig->isActiveAccount()) {
            return;
        }

        $shipmentFactory = new Shipment(
            $this->DPDconfig,
            (string) get_option('dpd_sender_city'),
            (string) $package['destination']['country'],
            (string) $package['destination']['state'],
            (string) $package['destination']['city'],
            (string) $package['cart_subtotal']
        );
        $shipmentFactory->setItemsByCart($package['contents']);
        $shipment = $shipmentFactory->getInstance();
        $converter = new Converter();

        $paymentMethod = false;

        if (isset($_REQUEST['payment_method'])) {
            //задаем оплату
            $shipment->setPaymentMethod(1, $_REQUEST['payment_method']);
            $paymentMethod = $_REQUEST['payment_method'];
        }

        try {
            //получаем способ доставки курьером
            $shipment->setSelfDelivery(false);
            $shipment->setSelfPickup(get_option('dpd_self_pickup'));
            $calc = $shipment->calculator();
            $calc->setCurrencyConverter($converter);
            $tariff = $calc->calculate(get_option('woocommerce_currency'));

            if ($shipment->isPossibileDelivery()) {
                $tariff = PriceRules::round($tariff);

                $this->add_rate([
                    'id' => $this->id.'_'.$tariff['SERVICE_CODE'].'_courier',
                    'label' => $this->method_title.' ('.(__('Courier', 'dpd')).')',
                    'cost' => $tariff['COST']
                ]);
            }

            //получаем способ доставки самовывоз
            $shipment->setSelfDelivery(true);
            $shipment->setSelfPickup(get_option('dpd_self_pickup'));
            
            $terminal = new Terminal($this->DPDconfig);
            $where = 'LOCATION_ID = :location AND SCHEDULE_SELF_DELIVERY != ""';
            $bind = [
                ':location' => $shipment->getReceiver()['CITY_ID']
            ];

            if (get_option('dpd_ogd')) {
                $where .= ' AND SERVICES LIKE :ogd';
                $bind['ogd'] = '%'.get_option('dpd_ogd').'%';
            }
            
            $nppPayment = get_option('dpd_commission_npp_payment');
            
            if ($nppPayment) {
                $nppPayment = unserialize($nppPayment);
            } else {
                $nppPayment = [];
            }
            
            if ($nppPayment && $paymentMethod && in_array($paymentMethod, $nppPayment)) {
                $where .= ' AND NPP_AVAILABLE =:npp';
                $bind['npp'] = 'Y';
            }

            $terminal->where($where, $bind);
            $terminals = Terminal::onlyAvaliable($terminal->get(), $shipment);

            if (sizeof($terminals) > 0) {
                $calc = $shipment->calculator();
                $calc->setCurrencyConverter($converter);
                $tariff = $calc->calculate(get_option('woocommerce_currency'));
            
                if ($shipment->isPossibileSelfDelivery()) {
                    $tariff = PriceRules::round($tariff);

                    $this->add_rate([
                        'id' => $this->id.'_'.$tariff['SERVICE_CODE'].'_pickup',
                        'label' => $this->method_title.' ('.(__('Pickup', 'dpd')).')',
                        'cost' => $tariff['COST']
                    ]);
                }
            }
            
        } catch(\SoapFault $e) {}
          catch(\Exception $e) {}
    }

    /**
     * Сохранение настроек
     * @return void
     */
    public function process_admin_options()
    {
        $importCompleted = get_option('dpd_first_data_import_completed');

        //валидация
        $dataForValidation = [
            [
                'type' => $importCompleted ? 'required' : '',
                'value' => isset($_POST['dpd']['sender_fio']) ?
                    sanitize_text_field($_POST['dpd']['sender_fio']) : '',
                'error' => __(
                    'Sender <strong>Contact person</strong> is required',
                    'dpd'
                )
            ],
            [
                'type' => $importCompleted ? 'required' : '',
                'value' => isset($_POST['dpd']['sender_name']) ?
                    sanitize_text_field($_POST['dpd']['sender_name']) : '',
                'error' => __(
                    'Sender <strong>Contact name</strong> is required',
                    'dpd'
                )
            ],
            [
                'type' => $importCompleted ? 'required' : '',
                'value' => isset($_POST['dpd']['sender_phone']) ?
                    sanitize_text_field($_POST['dpd']['sender_phone']) : '',
                'error' => __(
                    'Sender <strong>Phone</strong> is required',
                    'dpd'
                )
            ],
            [
                'type' => ($importCompleted ? 'email|required' : ''),
                'value' => isset($_POST['dpd']['sender_email']) ?
                    sanitize_email($_POST['dpd']['sender_email']) : '',
                'error' => __(
                    'Sender <strong>Email</strong> is required',
                    'dpd'
                )
            ],
            [
                'type' => $importCompleted ? 'required' : '',
                'value' => isset($_POST['dpd']['sender_city']) ?
                    sanitize_text_field($_POST['dpd']['sender_city']) : '',
                'error' => __(
                    'Sender <strong>City</strong> is required',
                    'dpd'
                )
            ],
            [
                'type' => $importCompleted ? 'required' : '',
                'value' => isset($_POST['dpd']['cargo_category']) ?
                    sanitize_text_field($_POST['dpd']['cargo_category']) : '',
                'error' => __(
                    '<strong>Sending content</strong> is required',
                    'dpd'
                )
            ],
            [
                'type' => 'number'. ($importCompleted ? '|required' : ''),
                'value' => isset($_POST['dpd']['sender_city_id']) ?
                    sanitize_text_field($_POST['dpd']['sender_city_id']) : 0,
                'error' => __(
                    'Use autocomplete for <strong>City</strong> field',
                    'dpd'
                )
            ],
            [
                'type' => 'number'. ($importCompleted ? '|required' : ''),
                'value' => isset($_POST['dpd']['weight_default']) ?
                    sanitize_text_field($_POST['dpd']['weight_default']) : 1000,
                'error' => __(
                    '<strong>Weight default, g</strong> can\'t be empty and must be a number',
                    'dpd'
                )
            ],
            [
                'type' => 'number'. ($importCompleted ? '|required' : ''),
                'value' => isset($_POST['dpd']['length_default']) ?
                    sanitize_text_field($_POST['dpd']['length_default']) : 200,
                'error' => __(
                    '<strong>Length by default, mm</strong> can\'t be empty and  must be a number',
                    'dpd'
                )
            ],
            [
                'type' => 'number'. ($importCompleted ? '|required' : ''),
                'value' => isset($_POST['dpd']['width_default']) ?
                    sanitize_text_field($_POST['dpd']['width_default']) : 100,
                'error' => __(
                    '<strong>Width by default, mm</strong> can\'t be empty and must be a number',
                    'dpd'
                )
            ],
            [
                'type' => 'number'. ($importCompleted ? '|required' : ''),
                'value' => isset($_POST['dpd']['height_default']) ?
                    sanitize_text_field($_POST['dpd']['height_default']) : 200,
                'error' => __(
                    '<strong>Height by default, mm</strong> can\'t be empty and must be a number',
                    'dpd'
                )
            ]
        ];

        if ($_POST['dpd']['account_default_country'] == 'RU') {
            $dataForValidation[] = [
                'type' => 'required',
                'value' => sanitize_text_field($_POST['dpd']['client_number_RU']),
                'error' => __('<strong>Client Number</strong> for Russia is required', 'dpd')
            ];

            $dataForValidation[] = [
                'type' => 'required',
                'value' => sanitize_text_field($_POST['dpd']['auth_key_RU']),
                'error' => __('<strong>Authorization key</strong> for Russia is required', 'dpd')
            ];

        } else if ($_POST['dpd']['account_default_country'] == 'KZ') {
            $dataForValidation[] = [
                'type' => 'required',
                'value' => sanitize_text_field($_POST['dpd']['client_number_KZ']),
                'error' => __('<strong>Client Number</strong> for Kazakhstan is required', 'dpd')
            ];

            $dataForValidation[] = [
                'type' => 'required',
                'value' => sanitize_text_field($_POST['dpd']['auth_key_KZ']),
                'error' => __('<strong>Authorization key</strong> for Kazakhstan is required', 'dpd')
            ];

        } else if ($_POST['dpd']['account_default_country'] == 'BY') {
            $dataForValidation[] = [
                'type' => 'required',
                'value' => sanitize_text_field($_POST['dpd']['client_number_BY']),
                'error' => __('<strong>Client Number</strong> for Belarus is required', 'dpd')
            ];

            $dataForValidation[] = [
                'type' => 'required',
                'value' => sanitize_text_field($_POST['dpd']['auth_key_BY']),
                'error' => __('<strong>Authorization key</strong> for Belarus is required', 'dpd')
            ];

        }

        if (isset($_POST['dpd']['self_pickup']) && $_POST['dpd']['self_pickup'] == 1) {
            $dataForValidation[] = [
                'type' => $importCompleted ? 'required' : '',
                'value' => isset($_POST['dpd']['sender_terminal_code']) ?
                    sanitize_text_field($_POST['dpd']['sender_terminal_code']) : 0,
                'error' => __('Sender <strong>Terminal</strong> is required', 'dpd')
            ];
        } else {
            $dataForValidation[] = [
                'type' => $importCompleted ? 'required' : '',
                'value' => isset($_POST['dpd']['sender_street']) ?
                    sanitize_text_field($_POST['dpd']['sender_street']) : '',
                'error' => __('Sender <strong>Street</strong> is required', 'dpd')
            ];
        }

        $errors = SimpleValidation::validate($dataForValidation);

        if ($errors) {
            $_SESSION['dpd_settings_notice'] = '<div class="error notice is-dismissible">';
            $_SESSION['dpd_settings_notice'] .= SimpleValidation::errorsHtml($errors);
            $_SESSION['dpd_settings_notice'] .= '</div>';
        } else {
            foreach ($_POST['dpd'] as $key => $value) {
                if (is_array($value)) {
                    $value = serialize($value);
                }
                update_option('dpd_'.$key, sanitize_text_field($value), true);
            }
            $params = [
                'ignore_tariff',
                'commission_npp_payment'
            ];
            foreach ($params as $param) {
                if (!isset($_POST['dpd'][$param])) {
                    update_option('dpd_'.$param, '', true);
                }
            }
            $_SESSION['dpd_settings_notice'] = '<div class="notice notice-success is-dismissible">';
            $_SESSION['dpd_settings_notice'] .= '<p>'.
                __('Your settings have been saved.', 'woocommerce').'</p>';
            $_SESSION['dpd_settings_notice'] .= '</div>';

        }
        return wp_redirect(
            home_url().'/wp-admin/admin.php?page=wc-settings&tab=shipping&section=dpd',
            301
        );
    }
}

?>