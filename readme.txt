=== Shipping calculator of DPD for WooCommerce ===
Contributors: dpdinrussia
Donate link: https://www.dpd.ru/
Tags: dpd, калькулятор доставки
Requires at least: 5.0.0
Tested up to: 5.0.3
Stable tag: 5.0.0
Requires PHP: 5.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Официальный модуль DPD. Добавляет на сайт автоматизированный расчет доставки через курьерскую службу DPD

== Description ==

Официальный модуль DPD.

DPD — это одна из крупнейших логистических компаний, которая осуществляет курьерскую доставку посылок по России и странам СНГ, с собственной сетью пунктов выдачи.

Модуль свяжет ваш интернет-магазин с личным кабинетом доставки DPD и вы получите следующие возможности:
*   Предоставление покупателю всех возможных способов доставки: курьером и на пункт выдачи заказов;
*   Возможность автоматического расчета стоимости доставки;
*   Вывод возможных вариантов доставки в зависимости от веса товара;
*   Вывод на страницу оформления заказа даты и стоимости доставки в зависимости от габаритов заказа;
*   Интерактивная карта для выбора пункта выдачи заказа при оформлении заказа, а также информационный виджет для раздела "Доставка" вашего интернет-магазина;
*   Изменять габариты заказа;
*   Передача заказа в личный кабинет DPD после его редактирования и подтверждения в панели администрирования 1С-Битрикс;
*   Создавать заявки на вызов курьера;
*   Печать квитанций к заказам и актов передачи курьеру из панели администрирования 1С-Битрикс;
*   Отслеживание статуса посылок в DPD*;
*   Синхронизация статусов доставки со статусами в панели администрирования 1С-Битрикс;
*   Автоматическая конвертация стоимости доставки из валюты ЛК DPD в валюту интернет-магазина при ее расчете;
*   Автоматическая конвертация сумм объявленной ценности и наложенного платежа из валюты интернет-магазина в валюту ЛК DPD по курсу на дату оформления заказа покупателем;
*   Поддержка мульти-аккаунтов для работы со странами СНГ;
*   Автоматический выбор подходящего аккаунта по стране назначения покупателя;

Модуль работает со следующими тарифами DPD
*   DPD OPTIMUM;
*   DPD Online Express;
*   DPD ECONOMY;
*   DPD ECONOMY CU;
*   DPD EXPRESS
*   DPD Online Max

Вы можете выбрать в настройках модуля, какие тарифы использовать в расчете. В этом случае покупателю будет отображаться оптимальный из возможных вариантов доставки.

* Для возможности отслеживания статусов посылок должна быть подключена услуга "Работа методом getStatesByClient с подтверждением". Подключение услуги нужно запросить на сайте DPD или у вашего персонального менеджера.

Примечание: к сожалению, из-за медленной работы сервера DPD, первичная загрузка страницы оформления заказа может занять некоторое время, в связи с временем необходимым на расчет стоимости доставки.

== Installation ==

1. Скачайте и распакуйте [архив](https://bitbucket.org/DPDinRussia/dpd.wordpress/get/master.zip) с модулем.
1. Его содержимое скопируйте в директорию /wp-content/plugins/woo-dpd/ Вашего сайта
1. Активируйте плагин на странице "Плагины" WordPress
1. Перейдите на страницу "WooCommerce / Настройки / Доставка / Доставка DPD" и произведите настройку плагина
1. После ввода данных авторизации выполните синхронизацию данных

= Обновление данных =

Для поддержания в актуальном состоянии данных о городах обслуживания и списка ПВЗ DPD
модулю необходимо обновлять свои данные. Для этого необходимо настроить запуск 
скрипта data-update.php с периодом раз в сутки.

Примерное задания для крона с запуском в 12ч ночи будет выглядеть следующим образом
0 0 * * * wget -O /dev/null http://ваш-сайт.ru/?action=dataUpdate, где  

Внимание! Запускать данное задание желательно во время наименьшей нагрузки на сайт, например
ночью.

= Обновление статусов =

Для работы механизма обновления и сихронизации статусов заказа необходимо настроить запуск
скрипта status-update.php с периодом раз в 10 минут 

Примерное задание для крона будет выглядеть следующим образом
*/10 * * * * wget -O /dev/null http://ваш-сайт.ru/?action=statusUpdate

== Frequently Asked Questions ==

= Не сохраняются настройки модуля =

Проверьте заполнение всех обязательных полей на странице.
В случае если какое-то поле пропущено - в верхней части страницы будет отображена ошибка

= Не удается выбрать город отправителя =

Запустите импорт данных заново. 
В случае если включен тестовый режим - отключите его и выполните сихронизацию заново.

= Не удается выбрать терминал =

Запустите импорт данных заново. 
В случае если включен тестовый режим - отключите его и выполните сихронизацию заново.

= Не отображается доставка =

Проверьте заполнен ли город отправителя. 

В случае если доставка выполняется от терминала, проверьте есть ли подходящий терминал в Вашем
городе отправления, в противном случае выберите доставку от двери

= Не соответсвует стоимость доставки расчитанной на сайте DPD =

Убедитесь, что на сайте DPD Вы выполняете расчет доставки по тем же параметрам, что и модуль.
На стоимоть доставки влияют такие параметры как

1. Габариты посылки (ширина, высота, длина)
1. Вес посылки
1. Объявленная ценность
1. Вид доставки (от терминала/двери до терминала/двери)

= Ничего не помогает =

В случае проблем с модулем обратитесь по адресу support@ipolh.com, описав суть проблемы и 
указав клиентский номер в системе DPD.

== Screenshots ==

1. /assets/screenshot/screenshot-1.png
1. /assets/screenshot/screenshot-2.png
1. /assets/screenshot/screenshot-3.png
1. /assets/screenshot/screenshot-4.png

== Changelog ==

= 1.0 =
* Выпуск модуля

== Upgrade Notice ==

= 1.0 =
* Выпуск модуля